<?php

declare(strict_types=1);

namespace Drupal\commerce_token_product\Token;

// Drupal modules.
use Drupal\commerce\Context;
use Drupal\commerce_price\Resolver\PriceResolverInterface;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_store\CurrentStoreInterface;
// Drupal core.
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Utility\Token;

/**
 * Provides price-related tokens for product entities.
 */
class ProductPrice {

  use StringTranslationTrait;

  /**
   * Constructs a new ProductPrice object.
   *
   * @param \Drupal\commerce_store\CurrentStoreInterface $currentStore
   *   The current store service.
   * @param \Drupal\commerce_price\Resolver\PriceResolverInterface $priceResolver
   *   The price resolver.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    protected CurrentStoreInterface $currentStore,
    protected PriceResolverInterface $priceResolver,
    protected Token $token,
    TranslationInterface $string_translation,
  ) {
    // Injections required by traits.
    $this->stringTranslation = $string_translation;
  }

  /**
   * Returns information about product price placeholder tokens we provide.
   *
   * @return array
   *   An associative array of available tokens and token types.
   *
   * @see hook_token_info()
   */
  public function getTokenInfo(): array {
    return [
      'tokens' => [
        'commerce_product' => [
          // A common use case for tokens that are replaced with a product's
          // lowest and highest price is in schema metatags for adding that
          // information to structured data in product offers. The values for
          // this use case is only relevant to search engines indexing
          // structured data on public pages as anonymous users. We therefore
          // provide tokens that are replaced with the calculated prices for
          // anonymous users specifically - even if the user visiting a page is
          // authenticated. This is to simplify caching. If the prices depend on
          // anything other the product's variations, such as on the domain in a
          // multi-domain setup, any appropriate cacheable dependencies should
          // be added via `hook_tokens_alter()`.
          // Similar tokens could be provided that would replace the tokens with
          // the calculated prices for the current user.
          'anonymous_calculated_low_price' => [
            'name' => $this->t('Anonymous calculated low price'),
            'description' => $this->t(
              'The lowest calculated price of any of the active variations of
               the product, as calculated for anonymous users. For use in
               structured data for search engines and other bots.',
            ),
          ],
          'anonymous_calculated_high_price' => [
            'name' => $this->t('Anonymous calculated high price'),
            'description' => $this->t(
              'The highest calculated price of any of the active variations of
               the product, as calculated for anonymous users. For use in
               structured data for search engines and other bots.',
            ),
          ],
        ],
      ],
    ];
  }

  /**
   * Returns replacement values for product price placeholder tokens we provide.
   *
   * @param string $type
   *   The machine-readable name of the type (group) of token being replaced.
   * @param array|mixed $tokens
   *   An array of tokens to be replaced.
   * @param array $data
   *   An associative array of data objects to be used when generating
   *   replacement values.
   * @param array $options
   *   An associative array of options for token replacement.
   * @param \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata
   *   The bubbleable metadata.
   *
   * @return array
   *   An associative array of replacement values.
   *
   * @see hook_tokens()
   */
  public function getTokens(
    $type,
    $tokens,
    array $data,
    array $options,
    BubbleableMetadata $bubbleable_metadata,
  ): array {
    if ($type !== 'commerce_product') {
      return [];
    }
    if (!isset($data['commerce_product'])) {
      return [];
    }

    [
      $price_tokens,
      $low_chained_tokens,
      $high_chained_tokens,
    ] = $this->getPriceTokens($tokens);

    $has_price_tokens = $this->hasPriceTokens(
      $price_tokens,
      $low_chained_tokens,
      $high_chained_tokens,
    );
    if ($has_price_tokens === FALSE) {
      return [];
    }

    [
      $prices,
      $cacheable_dependencies,
    ] = $this->getPricesAndDependencies($data['commerce_product']);

    $replacements = [];
    $replacements += $this->getParentTokenReplacements(
      $price_tokens,
      $prices,
    );
    $replacements += $this->getChainedTokenReplacements(
      $low_chained_tokens,
      $prices,
      'low',
    );
    $replacements += $this->getChainedTokenReplacements(
      $high_chained_tokens,
      $prices,
      'high',
    );

    foreach ($cacheable_dependencies as $dependency) {
      $bubbleable_metadata->addCacheableDependency($dependency);
    }

    return $replacements;
  }

  /**
   * Returns all price tokens contained in the main tokens array.
   *
   * @param array|mixed $tokens
   *   The array of tokens that are being replaced.
   *
   * @return array[]
   *   A numerical array containing the following items:
   *   - Tokens provided by this class.
   *   - Chained tokens derived from `anonymous_calculated_low_price`.
   *   - Chained tokens derived from `anonymous_calculated_high_price`.
   */
  protected function getPriceTokens($tokens): array {
    $price_tokens = \array_filter(
      $tokens,
      fn ($token, $key) => \in_array(
        $key,
        ['anonymous_calculated_low_price', 'anonymous_calculated_high_price'],
        TRUE,
      ),
      ARRAY_FILTER_USE_BOTH
    );
    $low_chained_tokens = $this->token->findWithPrefix(
      $tokens,
      'anonymous_calculated_low_price',
    );
    $high_chained_tokens = $this->token->findWithPrefix(
      $tokens,
      'anonymous_calculated_high_price',
    );

    return [
      $price_tokens,
      $low_chained_tokens,
      $high_chained_tokens,
    ];
  }

  /**
   * Returns whether we have price tokens based on the given groups.
   *
   * @param string[] ...
   *   Arrays containing the tokens, parent or chained, that are being replaced.
   *
   * @return bool
   *   `TRUE` if any of the given token groups have items, `FALSE` otherwise.
   */
  protected function hasPriceTokens(array ...$token_groups): bool {
    foreach ($token_groups as $group) {
      if (\count($group) > 0) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Returns the price token replacements and their cacheable dependencies.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The product.
   *
   * @return array
   *   A numerical array with the following two items in the given order:
   *   - An associative array containing the following items.
   *     - low: the lowest price of any of the product's active variations, or
   *       `NULL` if there is no active variation for the product.
   *     - high: the highest price of any of the product's active variations, or
   *       `NULL` if there is no active variation for the product.
   *   - The cacheable dependencies that affect price resolution for anonymous
   *     users.
   */
  protected function getPricesAndDependencies(
    ProductInterface $product,
  ): array {
    $variations = \array_values($product->getVariations());

    return [
      $this->getPrices($product, $variations),
      // The replacement values (prices) may change if any of the price-related
      // fields or the publication status of any of the product's variation
      // entities, active or inactive, change. We therefore return all the
      // variation entities as the cacheable dependencies. Any other
      // dependencies that might affect the calculated prices for anonymous
      // users should be added via `hook_tokens_alter()` by the module(s)
      // providing the relevant price resolvers.
      $variations,
    ];
  }

  /**
   * Returns an array with the lowest and highest price for the product.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The product.
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface[] $variations
   *   The product variations.
   *
   * @return array
   *   An associative array containing the following items:
   *   - low: the lowest price of any of the product's active variations, or
   *     `NULL` if there is no active variation for the product.
   *   - high: the highest price of any of the product's active variations, or
   *     `NULL` if there is no active variation for the product.
   */
  protected function getPrices(
    ProductInterface $product,
    array $variations,
  ): array {
    $prices = ['low' => NULL, 'high' => NULL];

    $variations = \array_filter(
      $product->getVariations(),
      fn ($variation) => $variation->isPublished(),
    );
    if (\count($variations) === 0) {
      return $prices;
    }

    $resolver = $this->priceResolver;
    $context = new Context(
      new AnonymousUserSession(),
      $this->currentStore->getStore(),
    );

    return \array_reduce(
      $variations,
      function ($carry, $variation) use ($resolver, $context) {
        $price = $resolver->resolve($variation, '1', $context);
        if ($carry['low'] === NULL || $price->lessThan($carry['low'])) {
          $carry['low'] = $price;
        }
        if ($carry['high'] === NULL || $price->greaterThan($carry['high'])) {
          $carry['high'] = $price;
        }
        return $carry;
      },
      $prices
    );
  }

  /**
   * Returns the token replacements for parent tokens.
   *
   * @param array $tokens
   *   The tokens.
   * @param array $prices
   *   An associative array containing the following items:
   *   - low: the lowest price of any of the product's active variations, or
   *     `NULL` if there is no active variation for the product.
   *   - high: the highest price of any of the product's active variations, or
   *     `NULL` if there is no active variation for the product.
   *
   * @return array
   *   An associative array containing the replacement values.
   */
  protected function getParentTokenReplacements(
    array $tokens,
    array $prices
  ): array {
    $replacements = [];
    foreach ($tokens as $name => $original) {
      $replacements[$original] = match ($name) {
        'anonymous_calculated_low_price' => $prices['low'] ?? '',
        'anonymous_calculated_high_price' => $prices['high'] ?? '',
      };
    }

    return $replacements;
  }

  /**
   * Returns the token replacements for chained tokens.
   *
   * The chained tokens we support is `number` and `currency_code` on the
   * `anonymous_calculated_low_price` and `anonymous_calculated_high_price`
   * tokens.
   *
   * @param array $tokens
   *   The chained tokens.
   * @param array $prices
   *   An associative array containing the following items:
   *   - low: the lowest price of any of the product's active variations, or
   *     `NULL` if there is no active variation for the product.
   *   - high: the highest price of any of the product's active variations, or
   *     `NULL` if there is no active variation for the product.
   * @param string $type
   *   The type of the given chained tokens. Supported types are `low` and
   *   `high`.
   *
   * @return array
   *   An associative array containing the replacement values.
   */
  protected function getChainedTokenReplacements(
    array $tokens,
    array $prices,
    string $type,
  ): array {
    if (!isset($prices[$type])) {
      // If we don't have the price i.e. there is no active variation for the
      // product - replace all tokens with an empty string.
      return \array_combine(
        \array_values($tokens),
        \array_fill(0, \count($tokens), ''),
      );
    }

    $replacements = [];
    foreach ($tokens as $name => $original) {
      $replacements[$original] = match ($name) {
        'number' => $prices[$type]->getNumber(),
        'currency_code' => $prices[$type]->getCurrencyCode(),
      };
    }

    return $replacements;
  }

}
